﻿.. Mascota documentation master file, created by
   sphinx-quickstart on Wed March 20 08:30:14 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentacion - Mascota
===========================

.. image:: _static/Mascotas.png
    :align: center
    :alt: Mascota


*Mascota* es app que crea una mascota virtual que 
puede **comer** y **jugar**, y sirve para que 
los niños se diviertan.

Esta compuesto de la clase ``Mascota``. La mascota normalmente posee:

- Nombre
- Edad
- Tipo
- Color

.. toctree::
   :maxdepth: 2
   :caption: Contenido:

.. automodule:: app
    :members:
    :special-members:



Índices y Tablas
================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
