"""
Creado el 20 de marzo de 2019
"""


class Mascota(object):
    """
    Crea una mascota
    """

    def __init__(self, n, e, t, c):
        """
        Constructor de la clase Mascota

        :param n: Nombre.
        :param e: Edad
        :param t: Tipo
        :param c: Color
        :type n: str
        :type t: str
        :type c: str
        :type e: int

        """
        self.nombre = n
        self.edad = e
        self.tipo = t
        self.color = c
        self.horajuga = 0

    def dormir(self):
        """
        La mascota esta durmiendo...
        """
        print("zzZzzZzZzz..")

    def jugar(self):
        """
        La mascota esta jugando...

        :return: Retorna la cantidad de horas jugadas
        :rtype: float

        """
        return self.horajuga


if __name__ == "__main__":
    n = input("Nombre: ")
    e = int(input("Edad: "))
    t = input("Tipo: ")
    c = input("Color: ")
    p = Mascota(n, e, t, c)

    print("Su mascota se llama: " +n)
    print("Su mascota es un@: " + t)
    print("Su mascota es de color: " + c)